#pragma once
#include "parser.h"

void register_constants() {
	parser_register_constant("ink_red", 0x00);
	parser_register_constant("ink_lime", 0x01);
	parser_register_constant("ink_blue", 0x02);
	parser_register_constant("ink_magenta", 0x03);
	parser_register_constant("ink_yellow", 0x04);
	parser_register_constant("ink_cyan", 0x05);
	parser_register_constant("ink_white", 0x06);
	parser_register_constant("ink_black", 0x07);
	parser_register_constant("ink_brown", 0x08);
	parser_register_constant("ink_green", 0x09);
	parser_register_constant("ink_navy", 0x0A);
	parser_register_constant("ink_purple", 0x0B);
	parser_register_constant("ink_gold", 0x0C);
	parser_register_constant("ink_teal", 0x0D);
	parser_register_constant("ink_gray", 0x0E);
	parser_register_constant("ink_black2", 0x0F);

	parser_register_constant("paper_red", 0x00);
	parser_register_constant("paper_lime", 0x10);
	parser_register_constant("paper_blue", 0x20);
	parser_register_constant("paper_magenta", 0x30);
	parser_register_constant("paper_yellow", 0x40);
	parser_register_constant("paper_cyan", 0x50);
	parser_register_constant("paper_white", 0x60);
	parser_register_constant("paper_black", 0x70);
	parser_register_constant("paper_brown", 0x80);
	parser_register_constant("paper_green", 0x90);
	parser_register_constant("paper_navy", 0xA0);
	parser_register_constant("paper_purple", 0xB0);
	parser_register_constant("paper_gold", 0xC0);
	parser_register_constant("paper_teal", 0xD0);
	parser_register_constant("paper_gray", 0xE0);
	parser_register_constant("paper_black2", 0xF0);
	
	parser_register_constant("btn_any", 0x00);
	parser_register_constant("btn_up", 0x01);
	parser_register_constant("btn_down", 0x02);
	parser_register_constant("btn_left", 0x03);
	parser_register_constant("btn_right", 0x04);
	parser_register_constant("btn_a", 0x05);
	parser_register_constant("btn_b", 0x06);
	parser_register_constant("btn_menu", 0x07);
}
