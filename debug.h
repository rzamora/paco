#pragma once
#include <SDL2/SDL.h>

void debug_init(unsigned char* mem, const bool window_shown = false);
void debug_update(const bool force = true);
void debug_hide();
void debug_mouse_event(SDL_Event& event);
const bool* debug_get_breakpoints();
