#pragma once

struct t_stack {
	unsigned char* data{ nullptr };
	int top{ 0 };
	int max{ 0 };
};

void				stack_init(t_stack& stack, const int size);
const bool			stack_isempty(t_stack& stack);
const bool			stack_isfull(t_stack& stack);
void				stack_push(t_stack& stack, const unsigned char value);
const unsigned char	stack_pop(t_stack& stack);
const unsigned char	stack_peek(t_stack& stack);
void				stack_delete(t_stack& stack);
