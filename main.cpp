#include <stdlib.h>
#include <stdio.h>
#include "vm.h"
#include "vdp.h"
#include "sound.h"
#include <SDL2/SDL.h>
#include "debug.h"

#define BTN_ANY		0
#define BTN_UP		1
#define BTN_DOWN	2
#define BTN_LEFT	3
#define BTN_RIGHT	4
#define BTN_A		5
#define BTN_B		6
#define BTN_MENU	7

bool anykey = false;
unsigned char data_in;

void input_data_in(const unsigned char& value) {
	data_in = value;
}

unsigned char input_data_out() {
	const Uint8* keys = SDL_GetKeyboardState(nullptr);
	switch (data_in) {
	case BTN_ANY: return anykey; break;
	case BTN_UP: return keys[SDL_SCANCODE_UP]; break;
	case BTN_DOWN: return keys[SDL_SCANCODE_DOWN]; break;
	case BTN_LEFT: return keys[SDL_SCANCODE_LEFT]; break;
	case BTN_RIGHT: return keys[SDL_SCANCODE_RIGHT]; break;
	case BTN_A: return keys[SDL_SCANCODE_RCTRL] || keys[SDL_SCANCODE_RALT]; break;
	case BTN_B: return keys[SDL_SCANCODE_RSHIFT] || keys[SDL_SCANCODE_RGUI]; break;
	case BTN_MENU: return keys[SDL_SCANCODE_RETURN]; break;
        default: return false;
	};
}

int main(int argc, char** argv) {

	// Inicialitzar la m�quina virtual (li arriba el arxiu amb el programa per linea de comandos)
	vm_init(argc < 2 ? nullptr : argv[1]); // "test.bas"

	// Inicialitzar el procesador gr�fic
	vdp_init();

	// Registrar els ports d'entrada i eixida del procesador gr�fic
	vm_register_out_port(10, vdp_data_out);
	vm_register_out_port(11, vdp_cmd_out);
	vm_register_in_port(12, vdp_in);

	// Registrar els ports d'entrada i eixida del dispositiu d'entrada (teclat, en cas del emu)
	vm_register_out_port(20, input_data_in);
	vm_register_in_port(21, input_data_out);

	// Inicialitzar el procesador de s�
	sound_init();

	// Registrar els ports d'entrada i eixida del procesador de s�
	vm_register_out_port(30, sound_data_out);
	vm_register_out_port(31, sound_cmd_out);
	vm_register_in_port(32, sound_in);

	// Inicialitzem el depurador del emulador
	debug_init(vm_get_memory());
	const bool* breakpoints = debug_get_breakpoints();

	bool running = true;

	static bool should_quit = false;
	static SDL_Event sdlEvent;
	SDL_Scancode just_pressed;

	//Uint32 ticks = SDL_GetTicks();
	while (!should_quit) {
		just_pressed = SDL_SCANCODE_UNKNOWN;
		while (SDL_PollEvent(&sdlEvent)) {
			switch (sdlEvent.type) {
			case SDL_WINDOWEVENT:
				switch (sdlEvent.window.event) {
				case SDL_WINDOWEVENT_CLOSE:
					should_quit = true; break;
				}
				break;
			case SDL_QUIT:
				should_quit = true; break;
			case SDL_KEYDOWN:
				//anykey = true;
				just_pressed = sdlEvent.key.keysym.scancode;
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_SPACE) { anykey = true; }
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE) { should_quit = true; }
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_F10) { if (!running) { vm_big_step(); debug_update(); } }
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_F11) { if (!running) { vm_step(); debug_update(); } }
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_F5) { running = !running; if (!running) debug_update(); else debug_hide(); }
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_F1) { vdp_switch_fullscreen(); }
				break;
			case SDL_KEYUP:
				if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_SPACE) { anykey = false; }
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEWHEEL:
				debug_mouse_event(sdlEvent);
				break;
			}
		}
		
		if (running) {
			unsigned short pc = vm_step();
			if (breakpoints[pc]) { running = false; debug_update(); }
		}
		//if (SDL_GetTicks() - ticks >= 15) { vdp_flip(); ticks = SDL_GetTicks(); }
	}

	vdp_quit();

	return 0;
}
