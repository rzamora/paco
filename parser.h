#pragma once

enum OPS {
	OP_NOP = 0,
	OP_PUSH,
	OP_POP,
	OP_DUP,
	OP_SWAP,

	OP_LOAD,
	OP_LOADI,
	OP_STORE,
	OP_STOREI,

	OP_LOADXY,
	OP_STOREXY,
	OP_SETX,
	OP_SETY,
	OP_SETZ,
	OP_GETY,
	OP_GETZ,
	OP_INCX,
	OP_DECX,
	OP_INCY,
	OP_DECY,
	OP_INCZ,
	OP_DECZ,

	OP_JMP,
	OP_JNT,
	OP_JTR,
	OP_JSR,
	OP_RET,
	OP_CALL,

	OP_RJ,
	OP_RB,
	OP_RJZ,
	OP_RJN,
	OP_RBZ,
	OP_RBN,

	OP_RJYZ,
	OP_RJYN,
	OP_RBYZ,
	OP_RBYN,
	OP_RJZZ,
	OP_RJZN,
	OP_RBZZ,
	OP_RBZN,

	OP_ADD,
	OP_SUB,
	OP_MUL,
	OP_DIV,
	OP_MOD,
	OP_AND,
	OP_OR,
	OP_NOT,
	OP_NEG,
	OP_INC,
	OP_DEC,
	OP_CONCAT,

	OP_EQ,
	OP_NEQ,
	OP_LT,
	OP_GT,
	OP_LEQ,
	OP_GEQ,

	OP_IN,
	OP_OUT,
	OP_SLEEP,
};

void parser_parse(const char* buffer, unsigned char* mem);
const int parser_get_codesize();
const int parser_get_memory_usage();
unsigned short* parser_get_lines();

void parser_register_external_function(const char* name, const int num_parameters);
void parser_register_constant(const char* name, const unsigned char value);