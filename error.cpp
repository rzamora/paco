#include "error.h"
#include <stdio.h>
#include "tokenizer.h"

static char errormsg[255];
static bool raised = false;


void error_raise(const char* msg) {
	if (!raised) {
		raised = true;
		sprintf(errormsg, "ERROR AT SOURCE:%s at %d:%d.", msg, tkn_get_line() + 1, tkn_get_row() + 1);
	}
}

bool error_raised() { return raised; }

void error_print(unsigned char* mem) {
    char* msg = errormsg;
    while (*msg != 0) *mem++ = *msg++;
	//printf("%s", errormsg);
}
