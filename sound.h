#pragma once

void sound_init();
void sound_load(const int channel, const char* string);
void sound_play();
const bool sound_isplaying();

void sound_data_out(const unsigned char& value);
void sound_cmd_out(const unsigned char& value);
unsigned char sound_in();
