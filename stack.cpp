#include "stack.h"
#include <stdlib.h>


#define TOP stack.data[0]
#define PUSH(x, y) x[++x[0]] = y
#define POP(x) (x[x[0]--])
#define PEEK(x) (x[x[0]])

void stack_push(t_stack& stack, const unsigned char value) { stack.data[++TOP] = value; }
const unsigned char stack_pop(t_stack& stack) { return stack.data[TOP--]; }
const unsigned char stack_peek(t_stack& stack) { return stack.data[TOP]; }

