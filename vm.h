#pragma once
//#include "stack.h"

void vm_init(const char* filename);
const unsigned short vm_step();
const unsigned short vm_big_step();
void vm_register_in_port(const unsigned char port, unsigned char(*callback)(void));
void vm_register_out_port(const unsigned char port, void(*callback)(const unsigned char&));
void vm_call_interrupt(const char num);

unsigned char* vm_get_memory();

//void vm_register_call(void(*callback)(t_stack&));
