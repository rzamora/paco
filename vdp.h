#pragma once

void vdp_init();
void vdp_quit();
void vdp_flip();
void vdp_switch_fullscreen();
void vdp_data_out(const unsigned char& value);
void vdp_cmd_out(const unsigned char& value);
unsigned char vdp_in();
